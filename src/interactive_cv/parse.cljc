(ns interactive-cv.parse
  (:require [clojure.string :as str]
            [clojure.walk :as walk]
            [clojure.zip :as z]
            [gnu.org-mode :as-alias org-mode]
            [interactive-cv.component :as-alias c]
            [interactive-cv.element :as-alias el]
            [interactive-cv.org-edn :as org]
            [interactive-cv.util :as u]
            [interactive-cv.zip :as cv.zip]))

(defn insert-post-blank [z]
  (let [{:keys [post-blank]} (some-> z z/node org/props)]
    (cond-> z
      (and (z/branch? z)
           (not (cv.zip/root? z))
           (some-> post-blank pos?))
      (z/insert-right
       (str/join (repeat post-blank " "))))))

(defn insert-pre-blank [z]
  (let [{:keys [pre-blank]} (some-> z z/node org/props)]
    (cond-> z
      (and (z/branch? z)
           (not (cv.zip/root? z))
           (some-> pre-blank pos?))
      (z/insert-left
       (str/join (repeat pre-blank " "))))))

(defn element->component [node]
  (let [t (when (isa? (org/node-type node) ::org-mode/headline)
            (org/prop node :todo-keyword))]
    (cond-> node t (org/with-type (keyword (namespace ::c/_) (name t))))))

(defn infer-type [node]
  (let [t (org/prop node ::org-mode/element-type)]
    (cond-> node
      (and (org/branch? node) t) (org/with-type (keyword (namespace ::org-mode/_)
                                                         (name t))))))

(defn infer-subtype [node]
  (if-let [t (org/prop node :type)]
    (org/with-subtype node t)
    node))

(defn props->map [node]
  (walk/postwalk #(cond-> %
                    (and (vector? %) ((set %) ::org-mode/element-type)) u/v->m)
                 node))
