(ns interactive-cv.org-edn
  (:require [clojure.zip :as z]
            [gnu.org-mode :as-alias org-mode]
            [interactive-cv.component :as-alias c]
            [interactive-cv :as-alias cv]
            [interactive-cv.element :as-alias el]
            [interactive-cv.zip :as cv.zip]
            #?(:cljs [json-html.core :refer [edn->hiccup]])))

(def branch? vector?)

(def children (fn [node] (seq (remove map? (rest node)))))

(def prop (fn [node k] (get (second node) k)))

(defn props [node]
  (cond-> node
    (branch? node) second))

(def make-node (fn [node children]
                 (into
                  (subvec node 0 (if (map? (second node)) 2 1))
                  children)))

(def zipper
  (partial z/zipper branch? children make-node))

(defn update-props [node f & args]
  (let [loc (-> node
                z/vector-zip
                z/down)]
    (z/root
     (if (some-> loc z/right z/node map?)
       (-> loc z/right (z/edit #(apply f % args)))
       (let [p (apply f {} args)]
         (cond-> loc (seq p) (z/insert-right p)))))))

(defn node-type [node]
  (when (branch? node) (first node)))

(def with-type #(assoc %1 0 %2))

(defn with-subtype [node k]
  (let [t (node-type node)]
    (with-type node (keyword (namespace t)
                             (str (name t) "." (name k))))))

(defn tag [node]
  (first (prop node :tags)))

(defn route [node]
  (when (or (isa? (node-type node) ::org-mode/headline)
            (= 'headline (prop node ::org-mode/element-type)))
    (tag node)))

(defn children->kv [node]
  (into {}
        (map #(do [(-> % :key keyword) (:value %)]))
        (map props (children node))))

(defn zip-walk [loc f & args]
  (zipper (apply cv.zip/walk loc f args)))

(defn seek-type [loc t]
  (some-> loc
          (cv.zip/seek #(isa? (node-type %) t))))

(defn seek-descendant [loc t]
  (some-> loc
          (cv.zip/seek #(isa? (node-type %) t))))

(defn seek-ancestor [loc t]
  (some-> loc
          (cv.zip/unseek #(isa? (node-type %) t))))

(defn some-node [loc t]
  (some-> loc (seek-descendant t) z/node))

(defn edit-descendant [loc t f & args]
  (let [ot (-> loc z/node node-type)]
    (-> loc
        (seek-type t)
        (z/edit #(apply f % args))
        (seek-ancestor ot))))

(defn drawer [loc]
  (seek-type loc ::org-mode/property-drawer))

(defmulti element first)

(def component element)

(defn org-element? [node]
  (and (keyword? (node-type node))
       (some-> node node-type namespace)))

(defmethod element :default [node]
  (if (org-element? node)
    [:<> [:strong (str node)]
     #_(:cljs [edn->hiccup node])]
    node))

(defmethod element ::org-mode/org-data [_] [:<>])

(defmethod element ::org-mode/section [_] [:<>])

(defmethod element ::org-mode/comment [_] [:<>])

(defmethod element ::org-mode/keyword [_] [:<>])

(defmethod element ::org-mode/paragraph [_] [:p])

(defmethod element ::org-mode/link.https [node]
  [:a {:href (prop node :raw-link)}])

(defmethod element ::org-mode/link.fuzzy [node]
  [:a {:href (prop node :raw-link)}])

(defmethod element ::paragraph [_] [:p])

(defmethod element ::text [node] (prop node :value))

(defmethod element ::italic [node] [:i (prop node :value)])

(defmethod element ::remove-me [node] (-> node meta :loc z/remove))

(defmethod element ::special-title [_] [:h1 {:class "special"}])

(defmethod element ::special-headline [node]
  (-> (:loc (meta node))
      z/down
      (z/edit with-type ::special-title)
      z/up
      (z/edit with-type :<>)))

(defmethod element ::org-mode/link.gitlab [[_ {:keys [path type]}]]
  [:a {:href (str "https://gitlab.com/" path)}
   [:span {:style {:background-color "#444"
                   :color "#ccc"
                   :padding "0 .4rem 0 .4rem"
                   :border-radius ".4rem 0 0 .4rem"}}
    (str "⇅ " type)]
   [:span {:style {:background-color "#ccc"
                   :padding "0 .4rem 0 .25rem"
                   :border-radius "0 .4rem .4rem 0"}} path]])

(def loc? (comp some? :zip/branch? meta))

(defn hiccup? [node]
  (when-let [t (some-> node first)]
    (or (symbol? t)
        (and (keyword? t)
             (not (namespace t))))))

(defn dissoc-org-keys [node]
  (update-props node dissoc :secondary-value? ::org-mode/element-type))

(defn el->hiccup [loc]
  (if (or (not (z/branch? loc))
          (hiccup? (z/node loc)))
    loc
    (let [result (-> (z/node loc)
                     (vary-meta assoc :loc loc)
                     element)]
      (when (:dbg (meta result)) (tap> result) (tap> loc))
      (cond
        (loc? result) (cond-> result (org-element? (z/node result)) z/prev)
        (branch? result) (-> loc
                             (z/replace (z/make-node loc result
                                                     (into (vec (children result))
                                                           (when-not (:drop-children (meta result))
                                                             (z/children loc)))))
                             (cond-> (org-element? result) z/prev))
        :else (z/replace loc result)))))

(def loc (comp :loc meta))

(defn annotate [loc]
  (cond-> loc
    (hiccup? (z/node loc)) (z/edit update-props assoc :data-annotate "Hi")))

(defn render [loc] (cv.zip/walk loc el->hiccup))
